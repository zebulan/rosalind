from bs4 import BeautifulSoup
from itertools import permutations, product
from requests import get
from re import compile, match


def multi_fasta(txt):  # Helper function <<<
    """
    :param txt: Multiple DNA strings in FASTA format
    :return: Dictionary of separate DNA strings (key = ID, value = DNA string)
    """
    d = dict()
    tmp = [a for a in txt.split('>') if a]
    for each in tmp:
        dna_id, *dna_str = each.split('\n')
        d[dna_id] = ''.join(dna_str)
    return d


def clean_fasta(fasta):  # Helper function <<<
    """ Extract only the DNA string from single FASTA entry """
    return ''.join(fasta.split('\n')[1:])


# ##################################################
def count_dna(txt):  # DNA: Counting the DNA Nucleotides
    """
    :param txt: DNA string of length at most 1000
    :return: Four integers counting number of times 'A G C T' appear in 'txt'
    """
    new = ''.join(x.strip() for x in txt.split('\n'))
    letters = ['A', 'C', 'G', 'T']
    letters_dict = {a: list() for a in letters}  # dict of lists
    [letters_dict[b].append(1) for b in new]     # lists of totals
    return ' '.join(str(sum(letters_dict[c])) for c in letters)

# dna_to_count = 'CCCCACGTACCGAGGTTGTCCCTCGTAATTCGCTGACCTCCTTGTCTTTCCATGCCATGCACTCCTTTCAGCCGAGTGAAATGATGTAATACCGAACGCCTTAGATCGCTACCCCCAATCCACCCGCTTTATTTGCGCCCTGACAAGTGCCCAGCTATACTATTTTACCACCCCCTGGCGAGACGGAAAAATACCCAGAAACGCCACCCACACAGTCATAATACGGGGACATTCTCGACCAACGCAATCTCATGTACGCACGACTTTTATTTGCTTCCAAGTCACGCGTACTAGCGTCAGGCCGCCGTAGTCATCCAGAACAAACACATACGAGCCACTTTGCCAGAGTCTCAGTCCGAGCCCTTAGTAACGTTTGGGCGACGTAACCGAGCCTCTACGCTGCTTCATACCAGGCAATCACACTTCTTTTTACGGGCTCACCTACGAACCGAAGTCTCACACGCAGAGAGAGTGGCCGAGGTTATATATCGCTAAGCAGACCTCTGAATGACCAGCATGTTTCACGTCCTCTATCGCGTAGCGGTCATCCTCGAGACATGGGCATTTGTGTTCTCCTAACAAATTCTGGTATTGCCGATCAAACTAAATCATCGATCCCTAGTAGACGGTCCCCCACGAATTTAAATATTCCGTCGAGACTTTTGACGATTGGGACTAATCTTCTTCCCTTTAGAATCAAAAATAAAACAACGAGGTAAAGGCAATTTATCAAAACGTGTCTTACATCCAAATATTGAACAGCGGTTGTCCGGGGGCATGCTAAGGATATCACATCGGCACGTGCGCATAGACTCGTGGATCCCCTCAGGAATTGATCTAGTGTGAATACCAGGTTGGTAACCTCTGATAAGCCTCCGACATAGTGAACGCCAATGGGCGATCTTGTACTTGTTGCGTGGAAATTACG'
# print(count_dna(dna_to_count))


def dna_to_rna(txt):  # RNA: Transcribing DNA into RNA
    """
    :param txt: DNA string of length at most 1000
    :return: Transcribed RNA string
    """
    return txt.replace('T', 'U')

# rna_to_switch = 'TCAATCGATTCCAAACTTTGTACGTGAGGCGGTACATCGACGGTCAATACTAATTTCGCTCGAGCGGGGAGCCGAGGAGACATGCTAGTAGTCCTGCCTTTTGTAGTGTTCTCGGGAGGAGAACGAAATTGTCTGCGTATGAAGGTGGGCGGACCCCTTGCACGGAATCGGAAGTCTTTTGCTGCTCAGGTCGAAGTATTTTGCTACGTGCACCTCAGCTCCCGCTTCGCCTGCGGGGCATGCATGTTTAAATGGCGCTCTTCCACTCAGGTCCTGCGAAGCTATTATAGTTGCCGCTAATACTTGTTGACCGCCCTGTTCGGAGTGTGATTTAGTATAGCTGGGCGTACAGCGTAGTACTCCTGTAGGGAATGCCGCTTATAATCCAGCACCTGGGTGCAGCTATCAACGAGTTAGCGATGTTCCGTCCATTCTCGCAAGACATGACAGTTGTTACGAAGGCATAACATTGAATCATTTCTCGCCTGAAATAAGGGCACCCACCCACTCACAGATACTATATGGGAGAATGCCGCAGGCAGCCTATCCTGAATCTTTCACGTACCGGGACTATTCGACTGCCAGTGCGTCTACCACTTGCTTGCGCTCAGTGGACTTGATAGTCTAGTGCAGGGGACTACGTCGGTATTTAGTTAAGAAAACTCAACGCCTTCAAGAATACCAGCAGTTCCCGGATGGTGGAGCAAACTACCTCATTTTTTTAGCGTTTAGCATCCTCTGCCCCATAAATCCGAGTGGACTATCTCAGGAAGCACAATCCTCGCTGTCCCGCTCCTTTTATCCAGGCGTCCTGCGGAGAAGTTCAACAGGATTTCACTAGGTCCCGGATCCAAAGATCCAATTCGGGTTACCCGCCCGTCGTTCGTGGGCTTTCGTTATAGTAAGAATA'
# print(dna_to_rna(rna_to_switch))


def rev_complement(txt):  # REVC: Complementing a Strand of DNA
    """
    :param txt: DNA string of length at most 1000
    :return: Reverse complement of 'txt' as a string (A <-> T, C <-> G)
    """
    dna_complements = {'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C'}
    return ''.join(dna_complements[a] for a in txt[::-1])

# dna = 'CGGCCTACCGAGAATAACATTCAGTGAAATGTCGTGTTCTGTCGTGGTGGGAGTTGCTGTACAATGCCCGGTACGCATATTGACCCTTATTGGCGCGATAGTTGGCCATGGGGTGCCAAGCTTTTACCAGTTACCCCCGTAGATTGTTTACCAGGCTAACATAGCAAAGTCCTGTGAACCTGTAGTCCCTAGCTCATTGGGCCGTTATATCGATACAGGGCACAGGGGATCACGTACCTCAGTTACGGCAACGGATGGACCGAGCGAGGAACCAATCAGCAACCACATATACAACAACACAAGGTGACTCCCCCCTTTACCATACCAGGGCAACACCCGGGGCGGACGTTCGTCGCTATGAATGGGACCGCCCTTATCGGGCGCGTAAACAGCAGATGTATCTTGATTCAGGGGGACACAGACTTATTTGCGTAATCGAAAAGGTTGGTCACCGATCCGACCAGAATACCTGAAGAGGGAACGTCGCCTAAGCGACTTCGTCGCACTCGCCTCCTAGCTGGGAGCTTTTGTACTCGGATTGAGCACAAGGTATTGTTATCGGCAAGCAAAGCCTTCGTACAGAGAGTTGATCTGAGACTACGATCCCTGCCGCAGGCCGGGTTCTACTCACGGATGAATTGTCTCCATGGGAATCATCTCTTCATCCCTGGAGGGCACCCGAATTCGCCAATGGATTAGTTTACATTGGTAGGACCATTGGCGCAGGGCCAACGTTGGACGCGTCGTTGTGCAGATAACGAACACTTCTCTGAAACCTTCATGCAACTTTTTTACCCGCCAACGATTTGAGCGCCACAAACTACTAATGGCCGTCGTTGGTTCTCTAGTACGGTCCCGAGGGCATGTATACCCTCGCCCCCT'
# print(rev_complement(dna))


def gc_content(txt):  # GC: Computing GC-content
    """
    :param txt: At most 10 DNA strings in FASTA format (<= 1 kbp each)
    :return: ID, GC-content of the string with highest GC-content
    """
    dna = dict()
    fasta_dict = multi_fasta(txt)
    for dna_id, dna_str in fasta_dict.items():
        gc = {'G': list(), 'C': list()}
        [gc[letter].append(1) for letter in dna_str if letter in gc]
        dna[((sum(gc['G']) + sum(gc['C'])) / len(dna_str)) * 100] = dna_id
    return '{0}\n{1:.6f}'.format(dna[max(dna)], max(dna))

# gc = '''\
# >Rosalind_6404
# CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC
# TCCCACTAATAATTCTGAGG
# >Rosalind_5959
# CCATCGGTAGCGCATCCTTAGTCCAATTAAGTCCCTATCCAGGCGCTCCGCCGAAGGTCT
# ATATCCATTTGTCAGCAGACACGC
# >Rosalind_0808
# CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGACCAGCCCGGAC
# TGGGAACCTGCGGGCAGTAGGTGGAAT'''
# # Rosalind_0808
# # 60.919540
# print(gc_content(gc))


def hamming(txt):  # HAMM: Counting Point Mutations
    """
    :param txt: Two DNA strings of equal length (<= 1 kbp each)
    :return: Integer of the Hamming distance
    """
    s, t = txt.split('\n')
    return sum(1 for x in zip(s, t) if len(set(x)) == 2)

# h = '''\
# ACCTGAGAAGGCATGTCGGCAATGGCGCGCCGCATTTGACCCACACTATTACATCCCCTGGGATTCTACGACTCCCGGACGGAAACAGTTAGAACTGTGTGATCCGCATATCTGAAGGGGGCCCCAGTGGTGGCTGTCGTCTGGGTCAGCGGGGTCAGCAACAACCTTCAGAAGTGGGCATCTTTCCCCGGGAACCTGGAGTGAGGAACTTTACGAATTGGAGATCACTAGTATCCTCCGGTGGCTCAATCACGTTTCTGAGCACTGTAGATCGTTTAAGCAAGATAAGCAAATGTCATAACACTAACCGTGGTGCATAATATACGCAATATCGGAGCGCGTCTGCTCTACTCCAACCAGGCTATCTCTATGGTCTTATTGCTAAAGCTTGACACTACGAAAGGGTTTATTTGGTAAGACTCGTGCTGGATTGGGAATGTACGATAGGACACATAGTCTATTGACCGCAACATTGATCGCTATTTCGGGGAATCCTTTCCGGACTCCCACAAACCGTTTAGTAAAACCAAAATTAGTTTAAGCGTCACCTCTTTACCAGGGCTCTGAAAATCACGGCACAGAGGGTGGAGGGTAACTTAAACGTAATAAAGTGGTACCCAGGGCTGTCCCCCCTCGCAATCGCACTAGTCTCTTAGGAAGTGGCAGTGTCCCCGGTAAATGGATCCAGCCGTCACCGTCTTAGCCAGTACGCTGACCGTCGGAATGTAAGATGGAAGTACTAGAGCATAAACTAGAATTCCCACTTGGCCTATGTCCCGCTAATCGCTGGCAGGACTTCATGGTCTGGCGCCGAGTGCGAAGGCAGTGACGGCCATCTCCTTGCATCTAAGATGGTGCGGTCCTTTTTGGGTTCGGACCACTTATGGAGCCTACGCGCCATGTCGACGACGCAACGGTCAAGGCGTTATAAGTGCG
# ACTCTCGACGCCGCAGCGCGAGTCCCTAACCCTGTTCGACATACACTAATACAGCCCGACCGACACCCGGAAGACCGCAGGGCGGCCAAGAGAACAGTATAGACCTGATATGTGAGGGGTGCGCCACATACGGGCTTCGATGGGTGGGTTGTGCTGAGTGTTAGGACGCATATCAATATGCCTCGTCGCGGATCCCTATAGAGACCTCCTATAAGAATTGCCCAAGAACACCCTGCCCGATTGGGTCGAGCACCTATTGGAAAACCGCTAAGCGCGGCGGGAATCCAGGCACCTTTGCCAACACGGATTGTTGTGCAAAATCTCCAGTATATCGTTGGGCCGCCGCGCGCTACCACCGACTCCAGCTATCCGGTCTGAATGATATTTGTGTCCTCCAAGTGAGGAATAATAATCGCGGACTAGCGTTAACTTGGGGATTTCTCGTATGCCAGACGTTTTAGACACCACAACGTTTTAAGCTAGTTGACGCCTTCGAGACTGGATTGCCACAATCGGACACCTCCATCCAGTTCTGTCATAAGTGCTAACTTTTTGCCAATGATTTGCCTAGCAACACATCAACCGTGCCGGCGAACAAAGATGACGGTACGAGCAACGCAGCCCTATCTGCCATCGCCAGCGGATGCCCATCAAAAGAATTTCGAGTCTCCTCGTCTAATGAGTCCAGCTGTTTCTGCCCGCGCTCGCAATTTGCTGGTGGCCCTGGAATGGTTGGCATCAGGGGACTCAATTGCACTTAAGACTTAGCTTACGTGCCGACAGGTTCAATCCTGACGTGGTTATCCCGTTCAAAAAGTTACAGCGGTGATGCCTATGTCCACCTACCTAGGCAGCTGGAGTCCTGATCAGGTTTGGACCTGCAATCGCTGTAACGCTGACAGGCGCCGGCTTATCCATGAATAAGGCATCAGGGTT'''
# print(hamming(h))


def rna_to_protein(txt):  # PROT: Translating RNA into Protein
    """
    :param txt: RNA string corresponding to a strand of mRNA (<= 10 kbp)
    :return: Encoded protein string
    """
    rna_codon = \
        {'UUU': 'F', 'UUC': 'F', 'UUA': 'L', 'UUG': 'L', 'UCU': 'S', 'UCC': 'S',
         'UCA': 'S', 'UCG': 'S', 'UAU': 'Y', 'UAC': 'Y', 'UAA': ' ', 'UAG': ' ',
         'UGU': 'C', 'UGC': 'C', 'UGA': ' ', 'UGG': 'W', 'CUU': 'L', 'CUC': 'L',
         'CUA': 'L', 'CUG': 'L', 'CCU': 'P', 'CCC': 'P', 'CCA': 'P', 'CCG': 'P',
         'CAU': 'H', 'CAC': 'H', 'CAA': 'Q', 'CAG': 'Q', 'CGU': 'R', 'CGC': 'R',
         'CGA': 'R', 'CGG': 'R', 'AUU': 'I', 'AUC': 'I', 'AUA': 'I', 'AUG': 'M',
         'ACU': 'T', 'ACC': 'T', 'ACA': 'T', 'ACG': 'T', 'AAU': 'N', 'AAC': 'N',
         'AAA': 'K', 'AAG': 'K', 'AGU': 'S', 'AGC': 'S', 'AGA': 'R', 'AGG': 'R',
         'GUU': 'V', 'GUC': 'V', 'GUA': 'V', 'GUG': 'V', 'GCU': 'A', 'GCC': 'A',
         'GCA': 'A', 'GCG': 'A', 'GAU': 'D', 'GAC': 'D', 'GAA': 'E', 'GAG': 'E',
         'GGU': 'G', 'GGC': 'G', 'GGA': 'G', 'GGG': 'G'}
    return ''.join([rna_codon[txt[a:a+3]] for a in range(0, len(txt), 3)])

# print(rna_to_protein('AUGGCCAUGGCGCCCAGAACUGAGAUCAAUAGUACCCGUAUUAACGGGUGA'))


def dna_motif(txt):  # SUBS: Finding a Motif in DNA
    """
    :param txt: Two DNA strings of length at most 1 kbp
    :return: String of index locations of each matching substring
    """
    s, t = txt.split('\n')
    t_len = len(t)
    return ' '.join([str(i + 1) for i, a in enumerate(s) if s[i:i+t_len] == t])

# f = '''\
# GATATATGCATATACTT
# ATAT'''
# print(dna_motif(f))


def overlap(txt):  # GRPH: Overlap Graphs
    """
    :param txt: A collection of DNA strings in FASTA format, length <= 10 kbp
    :return: Prints list of edges
    """
    fasta_dict = multi_fasta(txt)
    start, end = dict(), dict()
    for dna_id, dna_str in fasta_dict.items():
        start[dna_id] = dna_str[:3]  # first 3 characters
        end[dna_id] = dna_str[-3:]   # last 3 characters

    for k, v in start.items():
        for k2, v2 in end.items():
            if not k == k2 and v == v2:
                print(k2, k)

# dg = '''\
# >Rosalind_0498
# AAATAAA
# >Rosalind_2391
# AAATTTT
# >Rosalind_2323
# TTTTCCC
# >Rosalind_0442
# AAATCCC
# >Rosalind_5013
# GGGTGGG'''
# overlap(dg)


def find_shared_motif(txt):  # LCSM: Finding a Shared Motif
    """
    :param txt: A collection of DNA string in FASTA format (length <= 1 kbp)
    :return: A longest common substring of the collection (if multiple, any 1)
    """
    d = multi_fasta(txt)  # dict of FASTA strings -> {dna_id: dna_str}
    s_len, s_id, s_str = min((len(v), k, v) for k, v in d.items())  # shortest
    other_strings = [a for a in d.values() if not a == s_str]  # other dna_str
    common = set()

    for b in range(1, s_len):
        for i, c in enumerate(s_str):
            if i + b > s_len:
                break
            current = s_str[i:i+b]        # current slice of shortest string
            is_common = True
            for val in other_strings:     # check all other dna strings
                if current not in val:    # must be in ALL strings
                    is_common = False
                    break                 # no need to check further, next!
            if is_common:
                common.add((b, current))  # add common substring (len, string)
    return max(common)[1]                 # return any solution of max length

# test = '''\
# >Rosalind_1
# GATTACA
# >Rosalind_2
# TAGACCA
# >Rosalind_3
# ATACA'''
# print(find_shared_motif(test))


def protein_motif(txt):  # MPRT: Finding a Protein Motif
    """
    :param txt: At most 15 UniProt Protein Database access IDs
    :return: Print ID followed by a list of motif locations in protein string
    """
    url = 'http://www.uniprot.org/uniprot/'
    regex = compile(r'N[^P][ST][^P]')  # N{P}[ST]{P}... N-glycosylation motif
    for dna_id in txt.split('\n'):
        r = get(url + dna_id + '.fasta')
        soup = BeautifulSoup(r.text)
        dna_str = ''.join(soup.find('p').get_text().split('\n')[1:])
        tmp = list()
        for i, a in enumerate(dna_str):
            if match(regex, dna_str[i:i+4]):
                tmp.append(str(i + 1))
        if tmp:
            print('{}\n{}'.format(dna_id, ' '.join(tmp)))

# test = '''\
# A2Z669
# B5ZC00
# P07204_TRBM_HUMAN
# P20840_SAG1_YEAST'''
# protein_motif(test)


def enum_gene_orders(num):  # PERM: Enumerating Gene Orders
    """
    :param num: Positive Integer <= 7
    :return: Total number of permutations, followed by the list of permutations
    """
    p = list(permutations(range(1, num + 1)))
    print(len(p))
    [print(' '.join(str(y) for y in x)) for x in p]

# enum_gene_orders(3)


def protein_mass(protein):  # PRTM: Calculating Protein Mass
    """
    :param protein: A protein string of length at most 1000
    :return: Float of the total weight
    """
    weight = {'A': 71.03711, 'C': 103.00919, 'D': 115.02694, 'E': 129.04259,
              'F': 147.06841, 'G': 57.02146, 'H': 137.05891, 'I': 113.08406,
              'K': 128.09496, 'L': 113.08406, 'M': 131.04049, 'N': 114.04293,
              'P': 97.05276, 'Q': 128.05858, 'R': 156.10111, 'S': 87.03203,
              'T': 101.04768, 'V': 99.06841, 'W': 186.07931, 'Y': 163.06333}
    return round(sum(weight[x] for x in protein), 3)

# print(protein_mass('SKADYEK'))


def rev_palindrome(txt):  # REVP: Locating Restriction Sites
    """
    :param txt: DNA string of length at most 1 kbp in FASTA format
    :return: Position and Length of every reverse palindrome. (4 - 12 length)
    """
    dna_str = ''.join(a for a in txt.split('\n')[1:])
    dna_len = len(dna_str)
    final = list()
    for b in range(4, 13):  # 4 - 12 length chunks
        for c in range(0, dna_len + 1):
            if c + b == dna_len + 1:
                break
            check = dna_str[c:c+b]
            if check == rev_complement(check):  # 'check' is reverse palindrome
                final.append((c + 1, b))
    final.sort()  # sort is unnecessary, but matches their output
    [print(f, g) for f, g in final]

# fasta = '''\
# >Rosalind_24
# TCAATGCATGCGGGTCTATATGCAT'''
# rev_palindrome(fasta)


def rna_splicing(txt):  # SPLC: RNA Splicing
    """
    :param txt: DNA string and a collection of substrings acting as introns
    :return: Protein string from transcribing and translating the exons
    """
    fasta_dict = multi_fasta(txt)  # dictionary of all DNA strings
    dna_id = txt[1:txt.find('\n')]      # first DNA string (superset)
    dna_str = fasta_dict[dna_id]        # remove subset strings from this one
    for k, v in fasta_dict.items():
        if not k == v:                  # remove all substrings
            dna_str = dna_str.replace(v, '')
    return dna_str

# test = '''\
# >Rosalind_10
# ATGGTCTACATAGCTGACAAACAGCACGTAGCAATCGGTCGAATCTCGAGAGGCATATGGTCACATGATCGGTCGAGCGTGTTTCAAAGTTTGCGCCTAG
# >Rosalind_12
# ATCGGTCGAA
# >Rosalind_15
# ATCGGTCGAGCGTGT'''
# print(rna_to_protein(dna_to_rna(rna_splicing(test))))


def enum_k_mers(txt):  # LEXF: Enumerating k-mers Lexicographically
    """
    :param txt: A collection of at most 10 symbols defining an ordered
                alphabet and a positive int 'n' (<= 10)
    :return: Print all strings of length 'n' that can be formed from
             the alphabet, ordered lexicographically
    """
    dna_str, n = [a.replace(' ', '') for a in txt.split('\n')]
    [print(''.join(b)) for b in product(dna_str, repeat=int(n))]

# test = '''\
# T A G C
# 2'''
# enum_k_mers(test)


def spliced_motif(txt):  # SSEQ: Finding a Spliced Motif
    """
    :param txt: Two DNA strings, 's' and 't', in FASTA format (length <= 1 kbp)
    :return: One collection of indices of 's' in which the symbols
             of 't' appear as a subsequence of 's'. Finds the first
             matching sequence, returns as a space-delimited list of integers
    """
    s, t = (clean_fasta(a) for a in txt.split('>') if a)  # both DNA strings
    t_len = len(t)  # length of 't' for repeated checks below
    match_cnt = 0   # for the current index of 't', moves up with each match
    dex = list()    # list for the final subsequence of indices
    for i, a in enumerate(s):
        if a == t[match_cnt]:
            dex.append(str(i + 1))  # append index (+1) as a string
            match_cnt += 1          # start checking for next letter in 't'
            if match_cnt == t_len:  # if true, DONE!!!
                break
    return ' '.join(b for b in dex)

# test = '''\
# >Rosalind_14
# ACGTACGTGACG
# >Rosalind_18
# GTA'''
# print(spliced_motif(test))


def transitions_to_transversions(fasta):  # TRAN: Transitions and Transversions
    """
    :param fasta: Two DNA strings of equal length (at most 1 kbp)
    :return: The transition/transversion ratio
    """
    transition_cnt = 0
    transversion_cnt = 0
    transitions = {('A', 'G'), ('G', 'A'), ('C', 'T'), ('T', 'C')}
    for pair in zip(*list(multi_fasta(fasta).values())):
        if len(set(pair)) > 1:
            if pair in transitions:
                transition_cnt += 1
            else:
                transversion_cnt += 1

    return '{:.11f}'.format(transition_cnt / transversion_cnt)

# test = '''\
# >Rosalind_0209
# GCAACGCACAACGAAAACCCTTAGGGACTGGATTATTTCGTGATCGTTGTAGTTATTGGA
# AGTACGGGCATCAACCCAGTT
# >Rosalind_2200
# TTATCTGACAAAGAAAGCCGTCAACGGCTGGATAATTTCGCGATCGTGCTGGTTACTGGC
# GGTACGAGTGTTCCTTTGGGT'''
# print(transitions_to_transversions(test))


def k_mer_composition(fasta, k):  # KMER: k-Mer Composition
    """
    :param fasta: DNA string in FASTA format (length at most 100 kbp)
    :param k: Length of k-mer
    :return: The k-mer composition of DNA string 'fasta'
    """
    dna = clean_fasta(fasta)
    length = len(dna)
    d = {''.join(a): 0 for a in product('ACGT', repeat=k)}

    for i, char in enumerate(dna):
        if i + k > length:
            break
        d[dna[i:i+k]] += 1

    return ' '.join(str(d[key]) for key in sorted(d.keys()))

# test = '''\
# >Rosalind_6431
# CTTCGAAAGTTTGGGCCGAGTCTTACAGTCGGTCTTGAAGCAAAGTAACGAACTCCACGG
# CCCTGACTACCGAACCAGTTGTGAGTACTCAACTGGGTGAGAGTGCAGTCCCTATTGAGT
# TTCCGAGACTCACCGGGATTTTCGATCCAGCCTCAGTCCAGTCTTGTGGCCAACTCACCA
# AATGACGTTGGAATATCCCTGTCTAGCTCACGCAGTACTTAGTAAGAGGTCGCTGCAGCG
# GGGCAAGGAGATCGGAAAATGTGCTCTATATGCGACTAAAGCTCCTAACTTACACGTAGA
# CTTGCCCGTGTTAAAAACTCGGCTCACATGCTGTCTGCGGCTGGCTGTATACAGTATCTA
# CCTAATACCCTTCAGTTCGCCGCACAAAAGCTGGGAGTTACCGCGGAAATCACAG'''
# print(k_mer_composition(test, 4))


def diff_lengths(txt):  # LEXV: Order Strings of Varied Length Lexicographically
    """
    :param txt: A permutation of <= 12 symbols defining an ordered alphabet
                and a positive integer 'n' <= 4
    :return: Prints all strings of length at most 'n' formed from
             above alphabet, ordered lexicographically
    """
    letters, num = (''.join(a.split()) for a in txt.split('\n'))
    lex = dict()
    for b in range(1, int(num)+1):
        for c in product(letters, repeat=b):
            curr = ''.join(c)
            lex[curr] = list()
            if curr[:-1] in lex:
                lex[curr[:-1]].append(curr)

    def walker(key):  # Recursively walk through each key's value list
        if key:
            print(key)
            [walker(each) for each in lex[key]]

    [walker(e) for e in letters]

# test = '''\
# D N A
# 3'''
# diff_lengths(test)


# def increasing(txt):
#     pass
#
# test = '''\
# 5
# 5 1 4 2 3'''
# increasing(test)


# def seq_combos(text):  # REVERSE!!! <<<<<<<<<<<<<<<<<<<<<<<<
#     """ Slice string into all possible sequential chunks starting.
#         Returns a set of unique chunks of the string. """
#     end = 1                                # top end of slice
#     l = len(text)                          # length of string to split up
#     combos = set()                         # make sure no duplicates
#     while end < l:
#         start = 0                          # bottom end of slice
#         for x in range(l + 1):
#             chunk = text[start:start+end]  # slice of text string
#             if chunk:                      # make sure no blank entries
#                 combos.add(chunk)
#             start += 1                     # increment start of slice
#         end += 1                           # increment end of slice
#     return combos

# answer = sorted(set.intersection(*[seq_combos(x) for x in fasta_split(
#                 'rosa.txt').values()]), key=len, reverse=True)
# print(answer)


# # ###################################
# def signed_permutations(num):  # SIGN: Enumerating Oriented Gene Orderings
#     """
#     :param num: A positive integer (<= 6)
#     :return: Print the total number of permutations, followed by a list
#              of all such permutations (list can be in any order)
#     """
#     lst = list(range(1, num+1))  # [a for a in range(1, num+1)]  # 1 -> num
#     length = len(lst)
#     unsigned = list()
#     signed = list()
#     pos_neg = {a: {'neg': list(), 'pos': list()} for a in lst}
#     # print(pos_neg)
#     total = 0
#     for a in range(length + 1):
#         if a == 0:                       # unsigned 'lst' permutations
#             # [perm.append(' '.join(c)) for c in permutations(lst)]
#
#             # [unsigned.append(c) for c in permutations(lst)]
#
#             # for b in permutations(lst):
#             #     signed.append((''.join(str(c) for c in b), b))
#
#             for b in permutations(lst):
#                 pos_neg[b[0]]['pos'].append(b)
#                 total += 1
#
#         else:  # start by changing 1 sign at a time
#             for i, b in enumerate(lst):  # iterate through 'lst'
#                 tmp = lst[:]             # shallow copy of 'lst'
#                 if i + a > length:       # top end of slices
#                     break
#                 for c in range(i, i+a):  # iterate slices and change 'sign'
#                     # tmp[c] = ''.join(('-', tmp[c]))
#                     tmp[c] *= -1
#                 # [perm.append(' '.join(d)) for d in permutations(tmp)]
#
#                 #[signed.append(d) for d in permutations(tmp)]
#
#                 # for d in permutations(tmp):
#                 #     # two tuples???
#                 #     signed.append((''.join(str(abs(e)) for e in d), d))
#
#                 for d in permutations(tmp):
#                     pos_neg[abs(d[0])]['neg'].append(d)
#                     total += 1
#     print(pos_neg)
#     print()
#     print(total)
#     for e in lst:
#         for f in pos_neg[e]['neg']:
#             [print(g, end=' ') for g in f]
#             print()
#         for h in pos_neg[e]['pos']:
#             [print(i, end=' ') for i in h]
#             print()
#
#     # # print(len(perm))
#     # [print(x) for x in sorted(unsigned)]
#     # print()
#     # [print(x) for x in sorted(signed)]  # , key=lambda y: abs(y[0]))]
#
#     # print(len(signed))
#     # for e in sorted(signed):  # , key=lambda x: abs(x[0])):
#     #     [print(f, end=' ') for f in e[1]]
#     #     print()
# # sort all the items with itemgetter(loop through all!!!) ????
#
# signed_permutations(5)


# flip = \
#     {'M': ['AUG'],
#      'W': ['UGG'],
#      'N': ['AAC', 'AAU'],
#      'F': ['UUC', 'UUU'],
#      'H': ['CAC', 'CAU'],
#      'Y': ['UAC', 'UAU'],
#      'E': ['GAG', 'GAA'],
#      'D': ['GAC', 'GAU'],
#      'C': ['UGC', 'UGU'],
#      'Q': ['CAG', 'CAA'],
#      'K': ['AAA', 'AAG'],
#      ' ': ['UGA', 'UAA', 'UAG'],
#      'I': ['AUU', 'AUA', 'AUC'],
#      'T': ['ACA', 'ACC', 'ACU', 'ACG'],
#      'A': ['GCG', 'GCC', 'GCA', 'GCU'],
#      'G': ['GGA', 'GGU', 'GGG', 'GGC'],
#      'V': ['GUA', 'GUC', 'GUU', 'GUG'],
#      'P': ['CCG', 'CCC', 'CCU', 'CCA'],
#      'S': ['AGU', 'UCU', 'UCA', 'UCC', 'AGC', 'UCG'],
#      'L': ['UUG', 'CUC', 'CUA', 'CUU', 'UUA', 'CUG'],
#      'R': ['AGG', 'CGU', 'CGC', 'AGA', 'CGA', 'CGG']
#     }
#
# # for x in list('MA'):
# #     print(x, flip[x])
#
# test = 'AGCCATGTAGCTAACTCAGGTTACATGGGGATGACCCCGCGACTTGGATTAGAGTCTCTTTTGGAATAAGCCTGAATGATCCGAGTAGCATCTCAG'
# rna = dna_to_rna(test)
# rev_2_rna = dna_to_rna(rev_complement(test))
# print(rna)
# print(rev_2_rna)
# print()
# for i, a in enumerate(rna):
#     if rna[i:i+3] == 'AUG':
#         # print(i, rna[i:])
#         chunk = rna[i:]
#         # print(next(x in rna[i:] for x in ['UGA', 'UAA', 'UAG']))
#         for i2, b in enumerate(chunk):
#             if rna[i2:i2+3] in ['UGA', 'UAA', 'UAG']:
#                 print(chunk[:i2])
#
#                 # print(rna_to_protein(chunk[:i2]))
#                 print()
#                 break


# Genome Assembly as Shortest Superstring <-- LONG
test = '''\
>Rosalind_56
ATTAGACCTG
>Rosalind_57
CCTGCCGGAA
>Rosalind_58
AGACCTGCCG
>Rosalind_59
GCCGGAATAC'''

fasta = multi_fasta(test)

for k, v in fasta.items():
    # compare then save the key and score to a list???
    for k2, v2 in fasta.items():
        if k == k2:  # don't compare exact same dict entry
            continue
        else:  # compare all others, get ranking score?
            pass

